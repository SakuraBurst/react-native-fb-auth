/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable prettier/prettier */
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import auth from '@react-native-firebase/auth';
import Geolocation from '@react-native-community/geolocation';
import requestGeoPermission from '../geoRequest';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {getUserLocation} from '../actions/userLocation';
import {
  HeaderBackground,
  MainButton,
  ButtonText,
} from '../styledComponents/components';
import {Image, View} from 'react-native';

function Section({history}) {
  const dispatch = useDispatch();
  const geo = useSelector((state) => state.main.userLocation);
  useEffect(() => {
    requestGeoPermission();
    Geolocation.getCurrentPosition((info) => {
      dispatch(getUserLocation(info));
    });
  }, []);
  function signOut() {
    auth().signOut();
  }
  return (
    <View>
      <MapView
        style={{height: 330}}
        provider={PROVIDER_GOOGLE}
        region={{
          latitude: geo.latitude,
          longitude: geo.longitude,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        }}>
        <Marker
          coordinate={{
            latitude: geo.latitude,
            longitude: geo.longitude,
          }}>
          <Image
            source={require('../pictures/maps-and-flags.png')}
            style={{height: 32, width: 32}}
          />
        </Marker>
      </MapView>
      <HeaderBackground footer>
        <MainButton logout onPress={signOut}>
          <ButtonText logout>Понятно, я пойду</ButtonText>
        </MainButton>
      </HeaderBackground>
    </View>
  );
}
export default Section;

View;
