'use strict';
import {Image} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';
import {HeaderBackground, HeaderText} from '../styledComponents/components';

const Header = () => {
  const currentUser = useSelector(state => state.main.user);
  return (
    <HeaderBackground currentUser={!!currentUser}>
      <Image source={require('../pictures/LF.png')} />
      {currentUser && (
        <>
          <HeaderText mainT>Будем Знакомы:)</HeaderText>
          <HeaderText>Вы здесь</HeaderText>
        </>
      )}
    </HeaderBackground>
  );
};

export default Header;
