import React from 'react';
import {NavigationText, Container} from '../styledComponents/components';
import styled from 'styled-components';
import {Link} from 'react-router-native';

export default function Navigation({isActive}) {
  return (
    <NavigationContainer>
      <Link to="login">
        <NavigationText active={isActive ? true : false}>Вход</NavigationText>
      </Link>
      <Link to="signup">
        <NavigationText active={!isActive ? true : false}>
          Регистрация
        </NavigationText>
      </Link>
    </NavigationContainer>
  );
}

const NavigationContainer = styled(Container)`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
