/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {Field, reduxForm} from 'redux-form';
import FormField from './formField';
import {
  CentredContent,
  Container,
  ButtonMain,
  ButtonText,
  MainText,
  ImagePickerComp,
  ImagePickRow,
} from '../styledComponents/components';
import FormCheckBox from './formCheckBox';
import {useSelector, useDispatch} from 'react-redux';
import auth from '@react-native-firebase/auth';
import {successAuth} from '../actions/Auth';
import {Image} from 'react-native';
import ImagePicker from 'react-native-image-picker';

let LFSignUpForm = (props) => {
  const dispatch = useDispatch();
  const [emailError, setEmailError] = useState('');
  const [avatar, setAvatar] = useState(null);
  const check = useSelector((state) => state.form.signUp.values);
  const handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        setAvatar(response.uri);
      }
    });
  };
  const {handleSubmit} = props;
  const submit = async (values) => {
    try {
      await auth().createUserWithEmailAndPassword(
        values.signUpEmail,
        values.signUpPassword,
      );
      dispatch(successAuth());
    } catch (error) {
      if (error.code === 'auth/email-already-in-use') {
        setEmailError('Пользователь уже существует!');
      } else if (error.code === 'auth/invalid-email') {
        setEmailError('Неправильный Email');
      } else {
        alert(error);
      }
    }
  };
  function dummy() {
    console.log('dummy');
  }
  return (
    <Container>
      <ImagePickRow>
        <MainText grey>Фото</MainText>
        <ImagePickerComp onPress={handleChoosePhoto}>
          <Image
            source={
              avatar === null
                ? require('../pictures/Vector.png')
                : {uri: avatar}
            }
            style={
              avatar !== null
                ? {width: '100%', height: '100%', borderRadius: 50}
                : {}
            }
          />
        </ImagePickerComp>
      </ImagePickRow>
      <Field
        keyboardType="email-address"
        label="Email"
        component={FormField}
        name="signUpEmail"
        autoCompleteType="email"
        error={emailError}
      />
      <Field
        keyboardType="default"
        label="Пароль"
        component={FormField}
        name="signUpPassword"
        autoCompleteType="password"
        secureTextEntry={true}
        error=""
      />
      <CentredContent>
        <Field component={FormCheckBox} name="checkBox" type="checkbox" />
      </CentredContent>
      <CentredContent>
        <ButtonMain
          onPress={
            check === undefined
              ? dummy
              : check.checkBox
              ? handleSubmit(submit)
              : dummy
          }
          checkBox={check === undefined ? false : check.checkBox}>
          <ButtonText>Регистрация</ButtonText>
        </ButtonMain>
      </CentredContent>
    </Container>
  );
};

LFSignUpForm = reduxForm({
  form: 'signUp',
  initialValues: {
    checkBox: false,
  },
})(LFSignUpForm);

export default LFSignUpForm;
