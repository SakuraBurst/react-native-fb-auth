/* eslint-disable react-native/no-inline-styles */
import React, {useCallback} from 'react';
import {View, CheckBox, Linking} from 'react-native';
import {MainText} from '../styledComponents/components';

// взял валуе из пропсов, и не использую его, потому что с ним чекбокс тупо крашится
export default function FormCheckBox({
  input: {onChange, checked, value, ...restInput},
}) {
  const vk = 'https://vk.com';
  const extLink = useCallback(() => Linking.openURL(vk), [vk]);
  return (
    <View style={{flexDirection: 'row', justifyContent: 'center', width: 170}}>
      <CheckBox onValueChange={onChange} value={checked} {...restInput} />
      <MainText check>
        Я согласен с{' '}
        <MainText onPress={extLink} link>
          Политикой Конфиденциальнсти
        </MainText>
      </MainText>
    </View>
  );
}
