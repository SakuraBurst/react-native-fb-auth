/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {Field, reduxForm} from 'redux-form';
import FormField from './formField';
import {
  MainText,
  CentredContent,
  Container,
  MainButton,
  ButtonText,
} from '../styledComponents/components';
import {Link, Redirect} from 'react-router-native';
import auth from '@react-native-firebase/auth';
import {useSelector, useDispatch} from 'react-redux';
import {successAuth} from '../actions/Auth';

let LFLoginForm = (props) => {
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [disable, setDisable] = useState(false);
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.main.user);
  const {handleSubmit} = props;
  const submit = async (values) => {
    try {
      await auth().signInWithEmailAndPassword(
        values.logInEmail,
        values.logInPassword,
      );
      setDisable(true);
      dispatch(successAuth());
    } catch (error) {
      if (error.code === 'auth/invalid-email') {
        setEmailError('Неправильный Email');
        setPasswordError('');
      } else if (error.code === 'auth/wrong-password') {
        setPasswordError('Неправильный пароль');
        setEmailError('');
      } else if (error.code === 'auth/user-not-found') {
        setEmailError('Пользователь не существует');
        setPasswordError('');
      } else {
        alert(error);
      }
    }
  };
  if (currentUser) {
    return <Redirect to="/" />;
  }
  return (
    <Container>
      <Field
        keyboardType="email-address"
        label="Email"
        component={FormField}
        name="logInEmail"
        autoCompleteType="email"
        error={emailError}
        edit={!disable}
      />
      <Field
        keyboardType="default"
        label="Пароль"
        component={FormField}
        name="logInPassword"
        autoCompleteType="password"
        secureTextEntry={true}
        error={passwordError}
        edit={!disable}
      />

      <CentredContent>
        <MainText grey>Еще не зарегитированны</MainText>
        <Link to="/signup">
          <MainText>Регистрация</MainText>
        </Link>
      </CentredContent>
      <CentredContent>
        <MainButton
          onPress={disable ? () => console.log('nope') : handleSubmit(submit)}>
          <ButtonText>Войти</ButtonText>
        </MainButton>
      </CentredContent>
    </Container>
  );
};

LFLoginForm = reduxForm({
  form: 'login',
})(LFLoginForm);

export default LFLoginForm;
