/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Image, TouchableHighlight} from 'react-native';
import {
  InputComponent,
  MainText,
  ErorrText,
} from '../styledComponents/components';

export default function FormField({
  label,
  keyboardType,
  autoCompleteType,
  secureTextEntry,
  error,
  edit,
  input: {onChange, ...restInput},
}) {
  const [view, setView] = useState(secureTextEntry);
  function hide() {
    setView(!view);
  }
  return (
    <View>
      <View>
        <MainText grey>{label}</MainText>
        <InputComponent
          keyboardType={keyboardType}
          autoCompleteType={autoCompleteType}
          secureTextEntry={view}
          onChangeText={onChange}
          editable={edit}
          {...restInput}
        />
        {label === 'Пароль' && (
          <TouchableHighlight
            style={{position: 'absolute', right: 0, bottom: 10}}
            onPress={hide}>
            <Image source={require('../pictures/Browse.png')} />
          </TouchableHighlight>
        )}
      </View>
      {error.length > 0 && <ErorrText>{error}</ErorrText>}
    </View>
  );
}
