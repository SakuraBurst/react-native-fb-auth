/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {SafeAreaView, ScrollView, View, StatusBar} from 'react-native';

import Section from './components/section';
import Header from './components/header';
import {useDispatch} from 'react-redux';
import LFLoginForm from './components/loginForm';
import Navigation from './components/navig';
import {Switch, Route} from 'react-router-native';
import LFSignUpForm from './components/signUp';
import auth from '@react-native-firebase/auth';
import PrivateRoute from './routes/privateRoute';
import {setNewUser} from './actions/setUser';

const App = () => {
  const dispatch = useDispatch();
  function onAuthStateChanged(user) {
    dispatch(setNewUser(user));
  }
  useEffect(() => {
    auth().onAuthStateChanged(onAuthStateChanged);
  }, []);
  return (
    <>
      <StatusBar />
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <Header />
          <View>
            <Switch>
              <Route path="/login">
                <Navigation isActive={true} />
                <LFLoginForm />
              </Route>
              <Route path="/signup">
                <Navigation isActive={false} />
                <LFSignUpForm />
              </Route>
              <PrivateRoute exact path="/" component={Section} />
            </Switch>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default App;
