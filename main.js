import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './configureStore';
import {ConnectedRouter} from 'connected-react-router';
import history from './history';
import App from './App';
// Create redux store with history
const initialState = {};
const store = configureStore(initialState, history);

const Main = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  );
};

export default Main;
