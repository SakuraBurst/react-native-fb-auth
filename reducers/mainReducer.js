const initialState = {
  test: 'test react-redux',
  user: null,
  userLocation: {
    latitude: 55.798977,
    longitude: 49.106569,
  },
};
import {SETUSER} from '../actions/setUser';
import {LOCATION} from '../actions/userLocation';

export default function mainReducer(state = initialState, {type, payload}) {
  switch (type) {
    case SETUSER: {
      return {...state, user: payload};
    }
    case LOCATION: {
      return {
        ...state,
        userLocation: {
          latitude: payload.coords.latitude,
          longitude: payload.coords.longitude,
        },
      };
    }
    default:
      return state;
  }
}
