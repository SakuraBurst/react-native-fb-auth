export const SETUSER = 'SETUSER';

export const setNewUser = user => {
  return {
    type: SETUSER,
    payload: user,
  };
};
