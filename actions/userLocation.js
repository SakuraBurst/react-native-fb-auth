export const LOCATION = 'LOCATION';

export const getUserLocation = location => {
  return {
    type: LOCATION,
    payload: location,
  };
};
