import {push} from 'connected-react-router';

export const successAuth = () => {
  return dispatch => {
    dispatch(push('/'));
  };
};
