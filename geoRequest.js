import {PermissionsAndroid} from 'react-native';

const requestGeoPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Geo',
        message: 'Мне нужно знать где ты живешь(страшно вырубай)',
        buttonNeutral: 'Позже',
        buttonNegative: 'Отклонить',
        buttonPositive: 'Разрешить',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('Geo accepted');
    } else {
      console.log('Geo permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
};
export default requestGeoPermission;
