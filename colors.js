// default colors from newApp

export const Colors = {
  primary: '#1292B4',
  white: '#FFF',
  lighter: '#F3F3F3',
  light: '#DAE1E7',
  dark: '#444',
  black: '#000',
  lfBlue: '#3066E0',
  lfDarkBlue: '#252D76',
  lfLightBlue: '#2e64e1',
  lfGrey: '#AEAEAE',
  lfLightGrey: '#E7E7E7',
  lfRed: '#E81D4E',
};
