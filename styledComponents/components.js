/* eslint-disable prettier/prettier */
import styled from 'styled-components';
import {Colors} from '../colors';

export const CentredContent = styled.View`
  align-items: center;
  margin-top: 47;
`;

export const MainText = styled.Text`
  color: ${Colors.lfBlue};
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  ${(props) =>
    props.grey &&
    `
      color: ${Colors.lfGrey};
    `}
    ${(props) =>
      props.check &&
      `
        color: ${Colors.black};
      `}
      ${(props) =>
        props.link &&
        `
        font-weight: bold;
  `}
    
`;
export const Container = styled.View`
  margin-top: 32;
  padding-left: 24;
  padding-right: 24;
`;

export const InputComponent = styled.TextInput`
  border-bottom-width: 1px;
  border-bottom-color: rgba(37, 45, 118, 0.2);
  color: ${Colors.lfDarkBlue};
  font-weight: bold;
`;

export const NavigationText = styled.Text`
  font-weight: bold;
  font-size: 15px;
  color: ${Colors.lfGrey}
    ${(props) =>
      props.active === true &&
      `
      color: ${Colors.lfDarkBlue};
      font-size: 29px;
    `};
`;

export const HeaderBackground = styled.View`
  align-items: center;
  padding-bottom: 10;
  padding-top: 10;
  padding-horizontal: 32;
  background-color: ${Colors.lfBlue};
  ${(props) =>
    props.currentUser &&
    `
  justify-content: center;
  height: 250px
  `}
  ${(props) =>
    props.footer &&
    `
  height: 150px
  justify-content: center;
  `}
`;

export const HeaderText = styled(MainText)`
  color: ${Colors.white};
  position: absolute;
  bottom: 0;
  ${(props) =>
    props.mainT &&
    `
  position: relative;
  font-size: 30px;
  padding-top: 20px;
  `};
`;

export const MainButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  background-color: ${Colors.lfBlue};
  border-radius: 23px;
  height: 35.5;
  width: 109;
  text-align: center;
  ${(props) =>
    props.logout &&
    `
  background-color: ${Colors.white};
  width: 185;
  `}
  ${(props) =>
    props.checkBox === false &&
    `
    opacity: 0.2
  `}
`;

// тачбл опасити не обновляется при пропсах. точнее обновляется, но только при клике
export const ButtonMain = styled.TouchableHighlight`
  justify-content: center;
  align-items: center;
  background-color: ${Colors.lfBlue};
  border-radius: 23px;
  height: 35.5;
  width: 109;
  text-align: center;
  ${(props) =>
    props.checkBox === false &&
    `
    opacity: 0.2
  `}
`;

export const ButtonText = styled.Text`
  color: ${Colors.white};
  font-size: 14px;
  font-weight: bold;
  ${(props) =>
    props.logout &&
    `
  color: ${Colors.lfBlue};
  `}
`;

export const ErorrText = styled.Text`
  color: ${Colors.lfRed};
  font-size: 12px;
`;

export const ImagePickRow = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 62px;
  width: 240px;
`;

export const ImagePickerComp = styled.TouchableOpacity`
  background-color: ${Colors.lfLightGrey};
  width: 100px;
  height: 100px;
  border-radius: 50;
  justify-content: center;
  align-items: center;
`;
