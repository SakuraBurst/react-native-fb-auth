import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import mainReducer from './reducers/mainReducer';
import {connectRouter} from 'connected-react-router';
import history from './history';

export default function createReducer() {
  const rootReducer = combineReducers({
    main: mainReducer,
    form: formReducer,
    router: connectRouter(history),
  });
  return rootReducer;
}
